#include <stdio.h>
#include "embedded.h"
#include "controller.h"
#include "spiHelper.h"
#include "transmitBytes.h"
#include "ps2Interface.h"

static ps2ConfigBytes doTransmission(Controller *controller);
static void updateConfig(ps2ConfigBytes *configBytes, Controller *controller);

void playstation2Loop(Controller *controller)
{
    ps2ConfigBytes config = doTransmission(controller);
    updateConfig(&config, controller);
}

static ps2ConfigBytes doTransmission(Controller *controller)
{
    byte i;
    byte control;
    
    byte commandByte;
    byte nextDataByte;
    ps2ConfigBytes receivedBytes = {
        .command = 0,
        .config = {0}};

    // ************************************
    // *
    // * Transferring 3 header bytes
    // *
    // ************************************

    // Transfer header byte 1 - no data byte here
    commandByte = readCommand();
    if (commandByte != 0x01)
    {
        //Do something
    }

    // Determine header byte 2
    if (controller->config.mode)
    {
        nextDataByte = 0xF3;
    }
    else if (controller->config.analogue)
    {
        nextDataByte = 0x70 + controller->config.wordsInTransmission;
    }
    else
    {
        nextDataByte = 0x41;
    }
    
    // Transfer header byte 2
    commandByte = transferByte(nextDataByte);
    // Handle command byte 2 and determine header byte 3
    receivedBytes.command = commandByte & 0x0F;
    
    transmitBytes transmissionData = handleControlByte(receivedBytes.command, controller);
    
    nextDataByte = 0x5a;

    // Transfer header byte 3, command byte 3 is irrelevant
    commandByte = transferByte(nextDataByte);

    // ********************************************
    // *
    // * Transferring data and config bytes
    // *
    // ********************************************

    for (i = 0; i < transmissionData.length; i++)
    {   
        // determine data byte i
        nextDataByte = transmissionData.contents[i];

        // transfer data byte i
        commandByte = transferByte(nextDataByte);

        // Handle config byte i - if it's one of the first 6 bytes it's useful
        if (i < 6)
        {
            receivedBytes.config[i] = commandByte;
        }
    }
    return receivedBytes;
}

static void updateConfig(ps2ConfigBytes *configBytes, Controller *controller)
{
    byte command = configBytes->command;
    byte i;
    
    switch (command)
    {
    case 0x2:
        for (i = 0; i < 4; i++)
        {
            switch (controller->config.motorBytes[i])
            {
            case 0x00:
                controller->motors.smallMotor = configBytes->config[i];
                break;
            case 0x01:
                controller->motors.largeMotor = configBytes->config[i];
                break;
            default:
                break;
            }
        }
        break;
    case 0x3:
        controller->config.mode = (configBytes->config[0] && 0x01);
        byte buttonMaskSize;
        for (i = 0; i < 8; i++)
        {
            buttonMaskSize += controller->config.buttonMask[0] >> i & 0x01;
            buttonMaskSize += controller->config.buttonMask[1] >> i & 0x01;
            buttonMaskSize += controller->config.buttonMask[2] >> i & 0x01;
        }
        controller->config.buttonMaskSize = buttonMaskSize;
        controller->config.wordsInTransmission = buttonMaskSize / 2 + buttonMaskSize % 2;
        break;
    case 0x4:
        controller->config.analogue = (configBytes->config[0] && 0x01);
        controller->config.locked = (configBytes->config[1] && 0x03);
        break;
    case 0xD:
        for (i = 0; i < 4; i++)
        {
            controller->config.motorBytes[i] = configBytes->config[i];
        }
        break;
    case 0xF:
        controller->config.buttonMask[0] = configBytes->config[0];
        controller->config.buttonMask[1] = configBytes->config[1];
        controller->config.buttonMask[2] = configBytes->config[2];
        break;
    default:
        break;
    }

    return;
}