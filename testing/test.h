#ifndef TEST_H
#define TEST_H

typedef struct teensyTest_s
{
    int length;
    byte input[21];
    byte response[21];
    char error[100];
} teensyTest;

typedef struct TestBuffer {
    byte point;
    byte array[21];
} buffer;

bool runTest(teensyTest *test, Controller *controller);

#endif