#define TESTING

#include <stdio.h>
#include <stdbool.h>
#include "../embedded.h"
#include "../controller.h"
#include "../ps2Interface.h"
#include "./test.h"



buffer g_commandBuffer = {
    .point = 0};

buffer g_dataBuffer = {
    .point = 1,
    .array = {0xff}};

int main() 
{
    teensyTest start = {
        .length = 5,
        .input = {0x01, 0x42, 0x00, 0xff, 0xff},
        .response = {0xff, 0x41, 0x5a, 0xff, 0xff},
        .error = "failed to start"};

    teensyTest enterConfig = {
        .length = 5,
        .input = {0x01, 0x43, 0x00, 0x01, 0x00},
        .response = {0xff, 0x41, 0x5a, 0xff, 0xff},
        .error = "failed to enter config"};

    teensyTest enableAnalog = {
        .length = 9,
        .input = {0x01, 0x44, 0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00},
        .response = {0xff, 0xf3, 0x5a},
        .error = "failed to enable analog"};

    teensyTest setupMotors = {
        .length = 9,
        .input = {0x01, 0x4d, 0x00, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff},
        .response = {0xff, 0xf3, 0x5a, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
        .error = "failed to enable motors"};

    teensyTest enablePressure = {
        .length = 9,
        .input = {0x01, 0x4f, 0x00, 0xff, 0xff, 0x03},
        .response = {0xff, 0xf3, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x5a},
        .error = "failed to enable pressures"};

    teensyTest exitConfig = {
        .length = 9,
        .input = {0x01, 0x43, 0x00, 0x00, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a},
        .response = {0xff, 0xf3, 0x5a},
        .error = "failed to exit config"};

    teensyTest pollAll = {
        .length = 21,
        .input = {0x01, 0x42},
        .response = {0xff, 0x79, 0x5a, 0xff, 0xff, 0x7f, 0x7f, 0x7f, 0x7f},
        .error = "failed to poll controller"};

    Controller controller = initController();

    int numTests = 7;
    teensyTest *testList[] = {&start, &enterConfig, &enableAnalog, &setupMotors, &enablePressure, &exitConfig, &pollAll};


    printf("Starting\n");
    int i;
    for (i = 0; i < numTests; i++)
    {
        bool result = runTest(testList[i], &controller);
        if (!result) 
        {
            printf(testList[i]->error);
            return 1;
        }
    }
    return 0;
}

bool runTest(teensyTest *test, Controller *controller)
{
    int i;
    bool result = true;
    char *label = test->error+10;
    g_commandBuffer.point = 0;
    g_dataBuffer.point = 1;

    for (i = 0; i < 21; i++)
    {
        g_commandBuffer.array[i] = test->input[i];
    }
    
    playstation2Loop(controller);
    
    //Results
    for (i = 0; i < test->length; i++)
    {
        result = result & (g_dataBuffer.array[i] == test->response[i]);
    }

    //Logging
    printf("Command: ");
    printf(label);  
    printf("\n");   
    printf("Controller response: \n");
    printf("   actual: ");
    for (i = 0; i < test->length; i++)
    {
        printf("%.2x ", g_dataBuffer.array[i]); 
    }
    printf("\n expected: ");
    for (i = 0; i < test->length; i++)
    {
        printf("%.2x ", test->response[i]); 
    }
    printf("\nagreement: ");
    for (i = 0; i < test->length; i++)
    {
        printf(" %d ", g_dataBuffer.array[i] == test->response[i]);
    }
    printf("\n\n");
    return result;
}
