#include <stdio.h>
#include "controller.h"
#include "transmitBytes.h"

static void initPressure(transmitBytes *transmission);
static void getButtonMask(transmitBytes *transmission, Controller *controller);
static void getControllerButtons(transmitBytes *transmission, Controller *controller);
static void setConfigMode(transmitBytes *transmission, Controller *controller);
static void setAnalogueMode(transmitBytes *transmission);
static void getControllerInfo(transmitBytes *transmission);
static void getConstants67C(transmitBytes *transmission, Controller *controller);
static void setMotorControl(transmitBytes *transmission, Controller *controller);
static void setButtonMask(transmitBytes *transmission);

transmitBytes handleControlByte(byte control, Controller *controller)
{
    byte i;
    transmitBytes bytesToTransmit = {
        .length = 6,    //Default length of a config packet
        .contents = {0} //Mostly the default for unused data bytes
    };
    switch (control)
    {
    // TODO: Figure out what this is?
    case 0x0:
        initPressure(&bytesToTransmit);
        break;
    case 0x1:
        getButtonMask(&bytesToTransmit, controller);
        break;
    case 0x2:
        getControllerButtons(&bytesToTransmit, controller);
        break;
    case 0x3:
        setConfigMode(&bytesToTransmit, controller);
        break;
    case 0x4:
        setAnalogueMode(&bytesToTransmit);
        break;
    case 0x5:
        getControllerInfo(&bytesToTransmit);
        break;
    //0x6, 0x7, and 0x6 responses are combined
    case 0x6:
    case 0x7:
    case 0xC:
        getConstants67C(&bytesToTransmit, controller);
        break;
    case 0xD:
        setMotorControl(&bytesToTransmit, controller);
        break;
    case 0xF:
        setButtonMask(&bytesToTransmit);
        break;
    default: // case 0x8, 0x9, 0xA, 0xB, 0xE:
        getControllerButtons(&bytesToTransmit, controller);
        break;
    }
    return bytesToTransmit;
}

static void initPressure(transmitBytes *transmission)
{
    transmission->contents[2] = 0x02;
    transmission->contents[5] = 0x5a;
}

static void getButtonMask(transmitBytes *transmission, Controller *controller)
{
    // This command transmits the analogue mask
    // Might jut want to send 0xFF 0xFF 0x03 always
    transmission->contents[0] = controller->config.buttonMask[0];
    transmission->contents[1] = controller->config.buttonMask[1];
    transmission->contents[2] = controller->config.buttonMask[2];
    transmission->contents[5] = 0x5a;
}

static void getControllerButtons(transmitBytes *transmission, Controller *controller)
{
    byte i;
    byte transmissionSize;
    byte contentIndex;
    byte buttonIndex;
    bool allAnalogueMask;
    bool analogueJoysticksMask;
    byte byte0 = controller->config.buttonMask[0];
    byte byte1 = controller->config.buttonMask[1];
    byte byte2 = controller->config.buttonMask[2];

    bool allAnalogue = (byte0 == 0xFF) & (byte1 == 0xff) & (byte2 = 0x03);
    bool joyAnalogue = (byte0 == 0x2F) & (byte1 == 0x00) & (byte2 = 0x00);

    // If in full analogue mode
    if (controller->config.analogue && allAnalogue)
    {
        transmission->length = 18;
        transmission->contents[0] = controller->input.digital[0];
        transmission->contents[1] = controller->input.digital[1];

        for (i = 0; i < 16; i++)
        {
            transmission->contents[i + 2] = controller->input.analogue[i];
        }
    }
    else if (controller->config.analogue & joyAnalogue)
    {
        transmission->length = 6;
        transmission->contents[0] = controller->input.digital[0];
        transmission->contents[1] = controller->input.digital[1];
        for (i = 2; i < 6; i++)
        {
            transmission->contents[i] = controller->input.analogue[i - 2];
        }
    }
    else if (controller->config.analogue)
    {
        contentIndex = 0;
        buttonIndex = 0;
        transmission->length = controller->config.buttonMaskSize;

        //Sending only enabled analogue bytes to the console, based on the mask
        for (i = 0; i < 8; i++)
        {
            if (byte0 >> i & 0x01)
            {
                if (i < 2)
                {
                    transmission->contents[contentIndex] = controller->input.digital[buttonIndex];
                }
                else
                {
                    transmission->contents[contentIndex] = controller->input.analogue[buttonIndex - 2];
                }
                contentIndex++;
            }
            buttonIndex++;
        }
        for (i = 0; i < 8; i++)
        {
            if (byte1 >> i & 0x01)
            {
                transmission->contents[contentIndex] = controller->input.analogue[buttonIndex - 2];
                contentIndex++;
            }
            buttonIndex++;
        }
        for (i = 0; i <= 1; i++)
        {
            if (byte2 >> i & 0x01)
            {
                transmission->contents[contentIndex] = controller->input.analogue[buttonIndex - 2];
                contentIndex++;
            }
            buttonIndex++;
        }
    }
    else
    {
        transmission->length = 2;
        transmission->contents[0] = controller->input.digital[0];
        transmission->contents[1] = controller->input.digital[1];
    }
}

static void setConfigMode(transmitBytes *transmission, Controller *controller)
{
    if (!controller->config.mode)
    {
        getControllerButtons(transmission, controller);
    }
    //otherwise the response is the default
}

static void setAnalogueMode(transmitBytes *transmission)
{
    //packet contents are default
}

static void getControllerInfo(transmitBytes *transmission)
{
    //The third byte here should be LED state?
    const byte constant[6] = {0x03, 0x02, 0x01, 0x02, 0x01, 0x00};
    byte i;

    for (i = 0; i < 6; i++)
    {
        transmission->contents[i] = constant[i];
    }
}

static void getConstants67C(transmitBytes *transmission, Controller *controller)
{
    //TODO: These are dualshock 2 constants, not wireless/guitar constants
    const byte const6a[6] = {0x00, 0x00, 0x00, 0x02, 0x00, 0x0a};
    const byte const6b[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x14};
    const byte const7a[6] = {0x00, 0x00, 0x02, 0x00, 0x00, 0x00};
    const byte constCa[6] = {0x00, 0x00, 0x00, 0x04, 0x00, 0x00};
    const byte constCb[6] = {0x00, 0x00, 0x00, 0x06, 0x00, 0x00};

    const byte *constant;
    byte i;

    switch (controller->config.lastConstantTransmitted)
    {
    case 0x00:
        constant = const6a;
        controller->config.lastConstantTransmitted = 0x6a;
        break;
    case 0x6a:
        constant = const6b;
        controller->config.lastConstantTransmitted = 0x6b;
        break;
    case 0x6b:
        constant = const7a;
        controller->config.lastConstantTransmitted = 0x7a;
        break;
    case 0x7a:
        constant = constCa;
        controller->config.lastConstantTransmitted = 0xCa;
        break;
    case 0xCa:
        constant = constCb;
        controller->config.lastConstantTransmitted = 0xCb;
        break;
    default:
        break;
    }

    for (i = 0; i < 6; i++)
    {
        transmission->contents[i] = constant[i];
    }
}

static void setMotorControl(transmitBytes *transmission, Controller *controller)
{
    // TODO: The controller might need/use four motor bytes/states, not just two?
    // The dualshock 2 only has two though, I think.
    byte i;

    for (i = 0; i < 4; i++)
    {
        transmission->contents[i] = controller->config.motorBytes[i];
    }

    for (i = 0; i < 2; i++)
    {
        transmission->contents[i + 4] = 0xff;
    }
}

static void setButtonMask(transmitBytes *transmission)
{
    transmission->contents[5] = 0x5a;
}
