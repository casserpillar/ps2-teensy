#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <stdbool.h> 
#include "embedded.h"

enum analogueButtonOrder
{
    // Ordering of pressures in Input.analogue
    rStickX = 0x04,
    rStickY = 0x08,
    lStickX = 0x10,
    lStickY = 0x20,
    dpadRight = 0x40,
    dpadLeft = 0x80,
    dpadUp = 0x101,
    dpadDown = 0x102,
    triangle = 0x104,
    circle = 0x108,
    cross = 0x110,
    square = 0x120,
    l1 = 0x140,
    r1 = 0x180,
    l2 = 0x201,
    r2 = 0x202
};

struct Input //used to be buttonSTate
{
    byte digital[2]; // Two bytes containing digital button states
    byte analogue[16];
};

struct Config //used to be configState
{
    bool mode;                    // True if the controller is set to Config mode
    bool analogue;                // Analogue: True, Digital: False
    bool locked;                  // True: Can't switch modes'
    bool guitarController;        // False: DualShock 2, True: Guitar Hero
    bool led;                     // Enables the LED
    byte buttonMaskSize;          // Number of enabled buttons
    byte buttonMask[3];           // is analogue byte 0:17 in transmission?
    byte motorBytes[4];          // 0x00 = small, 0x01 = large, 0xFF = none
    byte wordsInTransmission;     // Number of 16-bit words in the data packet to the ps2
    byte lastConstantTransmitted; // Simplifies the transmission
};

struct Motors // used to be motorState
{
    byte smallMotor; // Coerced to a boolean for a DS2 controller
    byte largeMotor; // Values below 0x40 default to off on the controller
};

typedef struct Controller
{
    struct Input input;
    struct Config config;
    struct Motors motors;
} Controller;

Controller initController();

#endif