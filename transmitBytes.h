#ifndef transmitBytes_H
#define transmitBytes_H

#include <stdbool.h>
#include "embedded.h"

typedef struct transmitBytes
{
    byte length;
    byte contents[18];
} transmitBytes;

transmitBytes handleControlByte(byte control, Controller *controller);

#endif