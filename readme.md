# Teensy library for communicating with a PS2

### Cassandra Fantastic

## Licence
I have no idea.

## Attributions
Using info from [here](http://store.curiousinventor.com/guides/PS2#mappings) currently, other sources to be added.

## Conduct

To be added properly, but for now [here](http://geekfeminism.wikia.com/wiki/Community_anti-harassment/Policy).

## testing

Tests are currently in `/testing/test.c`

The tests only operate on the logic implementation of the ps2 protocol.

On windows, at least, with gcc and libc and all that stuff: 
```
gcc ./testing/test.c spiHelper.c ps2Interface.c transmitBytes.c controller.c -o test.exe
```

## Details

This feels logically mostly complete, the missing parts are hooking it into the teensy hardware. 

*Implemented*:
    
Logical structure of the controller->PS2 interface. When queried, the controller will appropriately update its internal state and use the correct response. This has testing. 

*Not implemented*:

Init/SPI/USB - there's no real connection to hardware yet. This means there's lots of little dummy functions to implement when it is. 