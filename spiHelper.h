#ifndef SPIHELPER_H
#define SPIHELPER_H

typedef struct ps2ConfigBytes
{
    byte config[6];
    byte command;
} ps2ConfigBytes;

byte transferByte(byte dataByte);
bool getAtt();
byte readCommand();
void queueData(byte data);
void sendAck();

#endif

