#define TESTING

#include <stdbool.h>
#include "embedded.h"
#include "spiHelper.h"

#ifdef TESTING
    #include <stdio.h>
    #include "controller.h"
    #include "./testing/test.h"
#endif



byte transferByte(byte dataByte)
{
    byte commandByte;

    queueData(dataByte);
    sendAck();
    commandByte = readCommand();

    return commandByte;
}

bool getAtt()
{
    return false;
}

byte readCommand()
{
#ifdef TESTING
    extern buffer g_commandBuffer;
    g_commandBuffer.point += 1;
    byte commandByte =  g_commandBuffer.array[g_commandBuffer.point-1];
    return commandByte;
#endif

    return 0x02;
}

void queueData(byte data)
{
#ifdef TESTING
    extern buffer g_dataBuffer;
    g_dataBuffer.array[g_dataBuffer.point] = data;
    g_dataBuffer.point += 1;
#endif
}

void sendAck() {}

/*
1: reads configState.enabledAnalogueButtons
2: uses basically all of configState to determine which parts of 
    buttonState to send
3: sets configMode, receives values of state2
4: sets analogueMode, lockedMode
5: gets controllerType, ledEnabled
6, 7, C: Read a garbage constant
D: toggles and maps motor control
F: Sets enabledAnalogueButtons
8, 9, A, B, E: unused? can log for them
*/