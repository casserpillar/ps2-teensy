#include "embedded.h"
#include "controller.h"
#include "spiHelper.h"
#include "transmitBytes.h"
#include "ps2Interface.h"

void loop();
void sleepMicroseconds(byte us);
void doUsbCommunication(Controller *controller);

void loop()
{
    Controller controller = initController();

    if (!getAtt())
    {
        playstation2Loop(&controller);
    }
    doUsbCommunication(&controller);
    sleepMicroseconds(100);
}

// Sleep for 100us, approximately
void sleepMicroseconds(byte us) {}

void doUsbCommunication(Controller *controller) {}