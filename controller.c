#include <stdbool.h>
#include "embedded.h"
#include "controller.h"

Controller initController()
{
    Controller controller = {
        .input = {
            .digital = {0xFF, 0xFF},
            .analogue = {0x7f, 0x7f, 0x7f, 0x7f, 0}},
        .config = {.mode = false, .analogue = false, .locked = false, .guitarController = false, .led = false, .buttonMaskSize = 2, .buttonMask = {0x03, 0x00, 0x00}, .motorBytes = {0xff, 0xff, 0xff, 0xff}, .wordsInTransmission = 1},
        .motors = {.smallMotor = 0x00, .largeMotor = 0x00}};
    return controller;
}
